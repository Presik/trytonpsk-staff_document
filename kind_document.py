# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import ModelView, ModelSQL, fields


class KindDocument(ModelSQL, ModelView):
    'Kind Document'
    __name__ = 'staff.kind_document'
    name = fields.Char('Name', required=True)

    @classmethod
    def __setup__(cls):
        super(KindDocument, cls).__setup__()
        cls._order.insert(0, ('name', 'ASC'))
