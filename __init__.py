#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
# from .kind_document import KindDocument
import document


def register():
    Pool.register(
        # KindDocument,
        # Document,
        document.Employee,
        document.NotificationDocument,
        document.DocumentExpiryDateStart,
        document.StaffContract,
        module='staff_document', type_='model')
    Pool.register(
        document.DocumentExpiryDate,
        module='staff_document', type_='wizard')
    Pool.register(
        document.DocumentExpiryDateReport,
        module='staff_document', type_='report')
